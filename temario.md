# Edición académica con Markdown

---
**Contenido**

1. [Objetivos](#objetivos)
2. [Requisitos](#requisitos)
3. [Sesiones](#sesiones)
    1. [Sesión 1](#sesión-1-introducción)
    2. [Sesión 2](#sesión-2-markdown-y-su-potencial)
    3. [Sesión 3](#sesión-3-bibliografía-1)

---


## Especificaciones

| **Rubro**  |     **Descripción**   |
| :---       |                  ---: |
| *Imparte*  |      Alberto R. León  |
| *Horas*    |               40 hrs. |
| *Sesiones* | 10 sesiones sabatinas |

## Objetivos

Los objetivos del curso son que el alumno:

* entienda las estructuras que conforman a un texto
* use bases de datos bibliográficas
* cree documentos profesionales
* tenga un acercamiento a herramientas descentralizadas y de acceso libre y/o abierto

## Requisitos

0. Curiosidad y ánimo de aprender
1. Computadora personal (cualquier SO)
2. Un texto académico (documento de trabajo) en formato `.docx` que incluya:
  * Tablas
  * Gráficas/imágenes
  * Citas
  * Bibliografía
3. Software instalado en computadora personal:
  * Tex-Live/miktext
    - [Descarga](https://miktex.org/download)
  * Pandoc [link de descarga](https://pandoc.org/installing.html)
  * Zotero [link de descarga](https://www.zotero.org/download/)
  * Zotero Better Bibtex [link de descarga](https://gitlab.com/paratexto/edic-acad-markdown/blob/master/recursos/zotero-better-bibtex-5.1.176.xpi)
  * Cite-proc
  * Editor de texto (p. e. Vim, Sublime Text, Atom, etc.)
    - Sublime text [link de descarga](https://www.sublimetext.com/3)

## Sesiones

### Sesión 1: Introducción

1. Presentación
  * ¿Cómo está conformado un texto?: la materialidad textual
  * La representación visual (i): lo que pensamos que es
  * Propiedades del texto
    * Entidades intrínsecas del texto
    * Paratexto
    * Hipertexto
  * Implicación de la organización textual
  * La representación visual (ii): lo que realmente es
  * Formatos

2. Markdown: un lenguaje para todos
  * ¿Qué es, para qué, por qué y cómo?

>**Actividad 1**[^1]
>
>Identificar en el documento de trabajo las características de su texto y analizar cómo se marcarían en MD.

3. Gestión de bases de datos bibliográficas
 * Bibtex: fundamentos básicos
 * Zotero: organizar nuestras referencias para la investigación y nuestra vida
    * DOI
    * ISBN
    * URL
    * Bibtex en Google Scholar

>**Actividad 2**[^2]
>
>Con la lista de referencias del documento de trabajo, empezar la base de datos bibliográfica.


4. La terminal y Pandoc
  * Terminal/bash o el recuadro negro que usan los de sistemas
  * Cómo usar terminal/bash y no morir en el intento (unos cuantos comandos básicos)
  * Pandoc: la navaja suiza de los formatos de texto
    * conversión de archivos
    * comandos básicos de Pandoc


>**Actividad 3**[^3]
>
>Convertir el documento de trabajo a formato `.md`, `.html` y `.pdf` usando Pandoc.


**Tarea 1**

1. Convertir el documnto de trabajo `.docx` a `.md` con Pandoc
2. Marcar el texto `.md` con base en la entidad textual que este tenga (título, subtítulo, cita, listas, etc.)
3. Una vez marcado el texto `.md`, convertirlo a formatos `.docx`, `.html` y `.pdf` con Pandoc
4. Crear base de datos bibliográfica en Zotero

### Sesión 2: Markdown y su potencial

1. *Metadata porn*: YAML
    * Qué es YAML
    * Por qué es tan importante la Metadata

>**Actividad 4**
>
>Poner bloque de metadata en documento de trabajo md.


2. Elementos gráficos
   * Gestión de imágenes

>**Actividad 5**
>
>Colocar las imágenes del documento en el documento de trabajo md.

3. Elementos hipertxtuales
  * links internos y externos
  * notas al pie

>**Actividad 6**
>
>Poner en caso de ser necesario los links dentro del documento de trabajo.

4. Elementos de representación de fundamentos
    * Tablas

>**Actividad 7**
>
>Crear tablas en el documento de trabajo md.

5. Otros Elementos
    * Citas
    * Código
    * Formato de texto

>**Actividad 8**
>
>Identificar y marcar todos los diferentes elementos en el documento de trabajo en md.

**Tarea 2**

1. Terminar de editar el documento de trabajo en `.md`
2. Convertir con Pandoc el documento de trabajo en `.md` a formatos `.docx`, `.html` y `.pdf`
3. Tener lista la base de datos bibliográfica


### Sesión 3: Bibliografía 1

1. La importancia de saber qué es qué cuando organizamos nuestras referencias
    * Repaso a Zotero
    * Identificadores de referencias: conocer nuestros items

2. Un texto, dos caminos
  * Seleccionar bibliografía en bloque YAML
  * No cite (bibliografía sin control)
  * Cite (Bibliografía controlada)
    * marcación de referencias en Markdown

3. El *flow* de la citación: estilos
  * Diferentes estilos de citación

4. Conversión con pandoc y cite-proc
  * Integrar nuevos parámetros.

>**Actividad 9**
>
>1. Hacer una copia del documento de trabajo en `.md`
>
>2. Convertir con Pandoc uno de los documentos a `.pdf` con el parámetro 'no cite'.
>
>3. Marcar las referencias en el segundo archivo sin el parametro 'no cite' y convertir con pandoc el archivo a `.pdf`.
>
>3. Ver las diferencias.

**Tarea 3**

1. Marcar todo el documento con los Id's de las referencias en el documento de trabajo `.md`.
2. Convertir archivo a `.docx`, `.html` y `.pdf`.



---

## Notas

[^1]: Para esta actividad hay que tener instalado un editor como Atom o Sublime text. Se proporcionará material de apoyo.

[^2]: Es necesario tener instalado Zotero.

[^3]: Asegurarse de tener instalado Pandoc y Texlive.
