\documentclass[12pt,spanish,legalpaper,]{report}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\PassOptionsToPackage{hyphens}{url} % url is loaded by hyperref
\usepackage[unicode=true]{hyperref}
\hypersetup{
            pdftitle={¿Reescribir la historia?: apuntes sobre apocalipsis y decadentismo},
            pdfauthor={Alberto R. León},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[shorthands=off,main=spanish]{babel}
\else
  \usepackage{polyglossia}
  \setmainlanguage[]{spanish}
\fi
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\title{¿Reescribir la historia?: apuntes sobre apocalipsis y decadentismo\thanks{Gracias a mis asesores y a Dios.}}
\author{Alberto R. León}
\date{10-12-2019}

\begin{document}
\maketitle
\begin{abstract}
Este artículo fue publicado en 2013 en telecapita.org, ahora el sitio ya
no está activo, se realiza un rescate del texto ya que el autor
considera puede aportar algo.
\end{abstract}

{
\setcounter{tocdepth}{2}
\tableofcontents
}
\listoftables
\listoffigures
Habrá quienes crean el viejo cuento de Francis Fukuyama, quien vaticinó
---con una mirada muy aguda pero no ajena o distante a otras inferencias
catastrofistas de su época (y por lo tanto afín a la paranoia que en los
80 se vivía)--- el fin de la historia. El apunte que realiza en este
célebre ensayo es claro y puntual: el neoliberalismo y sistema
capitalista es el tope máximo por el que históricamente ha luchado el
hombre a lo largo y ancho de su historicidad, pero cabe mencionar que al
mismo tiempo es un ``argumento coladera'', es decir que tiene muchas
fugas que lo cuestionan y lo atraviesan sin necesariamente derribarlo,
al menos no frontalmente, puesto que no existe interés alguno en
demostrar que Fukuyama estaba mal, o mejor dicho no existe el interés de
argumentarlo en un largo aliento. ¿A quién le interesa hoy en día hacer
una historia totalitaria, llegar a decir La Verdad sobre tal o cuál
suceso o acontecimiento y plantear la neta más neta de un fenómeno
específico?

Fukuyama (\protect\hyperlink{ref-Fukuyama1992}{1992}) fue, tal vez, uno
de los últimos historiadores comprometidos que llegaron a pensar la
historia en términos de totalidad. Él no tiene la culpa, simplemente
después de esta sentencia viral, pegajosa, exquisita, que se ha colado
en nuestra fraseología de café (``el fin de la historia'', ``el fin del
arte'', ``el fin de los relatos'' y demás fines apocalípticos), a nadie
le importó seguir diciendo realidades (pensemos en que la filosofía
también pasó por este proceso después de la IIGM). Era evidente que un
mundo de globalización y de información repleta de \emph{spam} y
distracciones, traería consigo la caída de la visión positivista de la
historia.

Cuando yo iba en la primaria, en los 90, podíamos definir sencillamente
el término \emph{Historia} (con mayúsculas) como ``la disciplina que se
encarga del estudio del pasado para entender nuestro presente y
visualizar el futuro''. Sin duda esta definición lograba encasillar el
método cientificista que devino en la decadencia que décadas atrás la
ideología punk apuntó (``No hay futuro'') y que apenas poco más de una
década después Fukuyama montó en el \emph{stablishment} cultural y
social. La bonita definición que aborda planos diacrónicos y sincrónicos
para proyectar una visión de lo venidero se desmembraba. Al clausurar la
posibilidad de un \emph{futuro} (puesto que ``no hay futuro'' o ``se
acabó la historia'') se cuestionó profundamente la historicidad del
hombre en sí. ¿Si nuestro presente no permite visualizar un futuro
entonces no se ha entendido el pasado?

Dejemos de lado la última pregunta que puede o no contestar dentro de su
ociosidad, pero enfoquémonos en un término que llama poderosamente mi
atención: decadencia. Podemos entender decadencia en un aspecto mocho y
simplista como lo que define la Real Academia Española
(\protect\hyperlink{ref-RealAcademiaEspanola2020}{2020}):

\begin{quote}
(de decadente). 1. f. Declinación, menoscabo, principio de debilidad o
de ruina.\\
2. f. En historia o en arte período en el que esto sucede''
\end{quote}

Charles Baudelaire en sus estudios sobre Edgar Allan Poe escribe:
``\ldots{}el sol que golpeaba todo con su luz blanca y derecha pronto
inundará el occidente de colores variados. Y en los arabescos de este
sol agonizante, algunos espíritus poéticos encontrarán delicias nuevas''
(Baudelaire \emph{apud} Villiers de L'Isle-Adam et~al.,
\protect\hyperlink{ref-VilliersdeLIsle-Adam2007}{2007}). La luz es la
civilización, la realidad, lo pleno, lo incuestionable que se
desfragmentará o mutará o entrará en decadencia y dará paso a la
catástrofe, llamémosle arte, historia, sociedad; pero impulsará, al
mismo tiempo, nuevas formas de relación y de visión que serán el punto
de inicio de algo nuevo. Baudelaire, en el fragmento anterior, escribe
sobre la decadencia. Tomémosla entonces como un periodo de destrucción y
de nacimiento. Si aventuramos un poco más este sentido baudeleriano
sobre la decadencia, podemos rastrearlo hasta la antigua Grecia, pasando
por el medievo, en la figura de Dionisos, dios del vino, la fiesta y
también de la naturaleza y el sacrificio que implica muerte, pero
también resurrección (Véase Otto,
\protect\hyperlink{ref-Otto2017}{2017}).

Decadencia: muerte-resurreción, derrumbe-construcción, fin-inicio. La
parte que Fukuyama ve, y con la que se queda, es con la versión del
derrumbe, de la catástrofe, la visión del triunfo neoliberalista en la
historia del hombre, un estadio total del que no hay una salida y que es
por lo tanto incuestionable. Fukuyama, sin querer, o por lo menos no da
a entender que lo hizo, cuestiona de raíz el fracaso del método
positivista de la historia desde su nido.

Pero no olvidemos que toda construcción de sentido o expresión es
forzosamente lingüística, bajo el entendido de que hay un emisor que
formula un mensaje y un receptor que lo decodifica. La creación de una
Historia (con mayúsculas), aspiraba a la seguridad de hechos capaces de
hacer entender nuestro presente y poder inferir sobre el futuro. La base
de este algoritmo era el pasado y el mecanismo que movía esta idea era
el \emph{progreso}, la sucesión de estados que se van mejorando cada vez
más y que se supera periódicamente. Esta es la idea progresista, que
alcanzó un tope evidente (el triunfo neoliberalista) en la tesis de
Fukuyama.

El lunes 8 de abril del 2013,
\href{http://es.wikipedia.org/wiki/Wikileaks}{Wikileaks} libera una base
de datos llamada \href{http://search.wikileaks.org/plusd/}{Cables
Kissinger}, que contienen cerca de 1 707 500 documentos privados que se
emitieron entre 1973 y 1976 entre el secretario de estado del gobierno
de EEUU, en el periodo señalado, y diversos actantes políticos y
diplomáticos en el resto del mundo. Estos documentos son en extremo
escandalosos, pues a raíz de ellos se ventiló información secreta de los
distintos gobiernos que tuvieron relación con EEUU en este periodo, por
ejemplo que el Rey Carlos de España era un soplón o que, en México, el
expresidente López Portillo, además de soplón, era una persona poco
preparada en los cargos ocupados, entre ellos el de hacienda,
desempañados antes de su mandato. Así varios países han logrado entender
diferentes aspectos de sus situaciones, a raíz de esta liberación masiva
de datos.

Hay que entender o dejar claras dos cosas: en primer lugar no hay que
tomar la liberación de esta cantidad ingente de documentos como un hecho
coyuntural y noticia que caducará en unos cuantos días o semanas, sino
como una herramienta de reconstrucción para fortalecer trabajos de
investigación históricos y discursivos en general; herramientas que nos
permitan entender las diferentes aristas que a raíz de estos escritos
pudieran salir y el contraste evidente con la oficialidad. Segundo, un
replanteamiento de la historia (con minúscula) en sí, que sea una
herramienta de cuestionamiento que evidencia el discurso formado por
aquellos que se han encargado de forjar en letras de bronce la Historia
de su país, de su realidad o de su visión.

Este trabajo que Wikileaks libera es en sí mismo una forma de abordaje
para reentender y resignificar el quehacer histórico. Si la fórmula era
``la Historia es la disciplina que se encarga del estudio del pasado
para entender nuestro presente y visualizar el futuro'', podemos jugar
un poco con esta concepción y pasar a articularla como ``bajo el
presupuesto de que el futuro quedó atrás (otro lema punk) estudiar ese
futuro sirve para proyectar nuestro presente y entender el pasado''. La
historia como género discursivo, capaz de crear, a partir de la
devastación proclamada por Fukuyama, un nuevo paraje de posibilidades
que negocien ese estado totalitario, mas no por ello incuestionable, que
corresponde a la contemporaneidad que habitamos.

Quizá haya que considerar a la decadencia como un principio apocalíptico
de fin y comienzo, la luz baudeleriana que todo lo ilumina se
desfragmenta y da paso a los matices, al caos, al derrumbe, pero al
mismo tiempo al comienzo. Ser históricos no es estar bajo esa luz de una
historia pétrea e inmutable, llena de hechos y verdades, sino es, desde
un punto muy subjetivo, ser discurso y construirla junto a uno mismo,
desde un punto no lineal sino relacional, que nos permita proclamar una
deixis de sucesos que den un amplio abanico de posibilidades. Si el
método esta jodido, hay que reinventarlo, ¿cómo lograr esto?, cada quién
tendrá su manera, su forma y sobre todo su visión. Por último, no es
prudente caer en posiciones ahistóricas o prohistóricas de una forma
definitiva, insisto, es más interesante la relación que se puede
estableces entre una y otra.

\chapter*{Lista de Referencias}\label{lista-de-referencias}
\addcontentsline{toc}{chapter}{Lista de Referencias}

\hypertarget{refs}{}
\hypertarget{ref-Fukuyama1992}{}
Fukuyama, F. (1992). \emph{The End of History and the Last Man}. New
York: Macmillan.

\hypertarget{ref-Otto2017}{}
Otto, W. F. (2017). \emph{Dioniso}. Navarra: Herder Editorial.

\hypertarget{ref-RealAcademiaEspanola2020}{}
Real Academia Española. (2020). \emph{Diccionario de La Lengua
Española}. https://dle.rae.es/.

\hypertarget{ref-VilliersdeLIsle-Adam2007}{}
Villiers de L'Isle-Adam, A., Barbey d'Aurevilly, J., Gourmont, R. de,
Huysmans, J.-K., Iglesias, C., Lorrain, J., \ldots{} Schwob, M. (2007).
\emph{Antología del decadentismo: perversión, neurastenia y anarquía en
Francia, 1880-1900}. Argentina: Caja Negra.

\end{document}
