# Hoja de apoyo Markdown

Guía de elementos markdown:

## Encabezados

| Marca | Descripción | Ejemplo |
| :---: |    :---:    |  :---:  |
|  `#`  | Encabezado 1    | `# Título de trabajo` |
| `##` | Encabezado 2  | `## Sección/subtítulo`|
| `###` | Encabezado 3 | `### Subsección` |
| `####`| Encabezado 4 | `#### Cuarto nivel de texto` |
| `####`| Encabezado 5 | `##### Quinto nivel de texto`|
| `######`| Encabezado 6 | `###### Sexto nivel de texto` |

### Otros Encabezados[^1]

| Marca | Descripción |
| :---: |    :---:    |
| `==`  | Encabezado 1 |
| `--`  | Encabezado 2 |

## Listas

### Listas desordenadas

| Marca | Descripción |
| :---: |    :---:    |
| `*`, `-` o `+`  | Se deben colocar antes de cada *item* enlistado. |

#### Ejemplo:

````
* Ítem 1
+ Ítem 2
- Ítem 3
````
Produce:

* Ítem 1
- Ítem 2
+ Ítem 3

---
Nota: No importa el uso de `*`, `-` o `+`, es indistinto.

---

### Listas ordenadas

| Marca | Descripción |
| :---: |    :---:    |
| `1.`, `2.`, `3.`, etc.  | Se deben colocar antes de cada *item* enlistado. |

#### Ejemplo:

````
1. Ítem 1
2. Ítem 2
3. Ítem 3
````
Produce:

1. Ítem 1
2. Ítem 2
3. Ítem 3

### Listas anidadas

Estas pueden ser ordenadas, desordenadas o combinadas

#### Ejemplo:

````
1. Ítem 1
  1. Ítem 1.1
  2. Ítem 1.2
    * Ítem 1.2.1
    * ítem 1.2.2
  3. Ítem 1.3
    + Ítem 1.3.1
      1. Ítem 1.3.1.1
2. Ítem 2
3. Ítem 3
````
Produce:

1. Ítem 1
  1. Ítem 1.1
  2. Ítem 1.2
    * Ítem 1.2.1
    * ítem 1.2.2
  3. Ítem 1.3
    + Ítem 1.3.1
        1. Ítem 1.3.1.1
2. Ítem 2
3. Ítem 3

## Bloques

### Citas

Se emplea el símbolo `>` para señalar una cita, en caso de que tenga más de un párrafo o línea se debe señalar cada inicio de línea con este signo.

#### Ejemplo 1

````
>*Inútil*
>
>No por\
>Mucho\
>Publicar\
>Te consagras\
>Más\
>Temprano
>
> &mdash;E. Huerta
````
Produce:

>*Inútil*
>
>No por\
>Mucho\
>Publicar\
>Te consagras\
>Más\
>Temprano
>
> &mdash;E. Huerta

#### Ejemplo 2

Es posible realizar citas anidadas, estas se marcan con `>>`, el siguiente ejemplo es tomado de un artículo de Luz Aurora Pimentel:[^2]

````
>Ahora bien, si la descripción autoral pretende acceder al mundo como re-presentación, como copia de la realidad, como una estructura exterior objetiva y colectiva, en la situación narrativa figural, en cambio, el solo desplazamiento focal conlleva una asunción total de subjetividad. El mundo significado de esta manera podría caracterizarse con las mismas palabras que Merleau-Ponty utiliza para describir la experiencia subjetiva del espacio:
>
>>El espacio no es más aquel de que habla la Dióptrica, red de relaciones entre objetos, tal como lo vería un tercero, testigo de mi visión, o un geómetra que la reconstruye y la sobrevuela; es un espacio contado a partir de mí mismo como punto o grado cero de la espacialidad. Yo no lo veo conforme a su envoltura exterior, lo vivo adentro, estoy englobado en él (1986, 44).
````

Produce:

>Ahora bien, si la descripción autoral pretende acceder al mundo como re-presentación, como copia de la realidad, como una estructura exterior objetiva y colectiva, en la situación narrativa figural, en cambio, el solo desplazamiento focal conlleva una asunción total de subjetividad. El mundo significado de esta manera podría caracterizarse con las mismas palabras que Merleau-Ponty utiliza para describir la experiencia subjetiva del espacio:
>
>>El espacio no es más aquel de que habla la Dióptrica, red de relaciones entre objetos, tal como lo vería un tercero, testigo de mi visión, o un geómetra que la reconstruye y la sobrevuela; es un espacio contado a partir de mí mismo como punto o grado cero de la espacialidad. Yo no lo veo conforme a su envoltura exterior, lo vivo adentro, estoy englobado en él (1986, 44).

### Código

El código puede ser marcado en dos momentos:

* Como bloque
* Como elemento de línea

#### Código como Bloque

Se puede marcar con tres acentos invertidos o tres cedillas `~`

**Ejemplo**

~~~
```
blockquote  {
 	font-family: sans-serif;
	margin-top: 1em;
	margin-bottom:1em;
  	margin-left: 50px;
  	margin-right: 80px;
  	font-size: 15px;
  	text-align: justify;
  	line-height:15px;
	text-indent: 0;
	border-left-style:outset;
}
a {
	color: #1F4293;
}

```
~~~

Produce:

```
blockquote  {
 	font-family: sans-serif;
	margin-top: 1em;
	margin-bottom:1em;
  	margin-left: 50px;
  	margin-right: 80px;
  	font-size: 15px;
  	text-align: justify;
  	line-height:15px;
	text-indent: 0;
	border-left-style:outset;
}
a {
	color: #1F4293;
}

```
Asimismo pueden ser bloques de código enriquecidos, por ejemplo:

~~~
```ruby
require 'fileutils'

    product_name = "biblio";

    filenames = Dir.glob("*.bib")

    filenames.each do |filename|
      File.rename(filename, product_name + '.bib')
    end

    require 'fileutils'

    filenames = Dir.glob("*.docx")
```
~~~

Produce:

```ruby
require 'fileutils'

    product_name = "biblio";

    filenames = Dir.glob("*.bib")

    filenames.each do |filename|
      File.rename(filename, product_name + '.bib')
    end

    require 'fileutils'

    filenames = Dir.glob("*.docx")
```

## Elementos de línea

### Énfasis

| Marca | Resultado |
| :---: |   :---:   |
| `*cursiva*` | *cursiva* |
| `_cursiva_` | _cursiva_ |
| `**negrita**` | **negrita** |
| `__negrita__` | __negrita__ |
| `***curs. y negrita***` | ***curs. y negrita*** |
| `___curs. y negrita___` | ___curs. y negrita___ |
| `~tachado~`| ~tachado~ |

### Código en línea

| Marca | Resultado |
| :---: |   :---:   |
| ` ``código en línea`` ` | ``código en línea``|

### Saltos de línea, párrafos y versos

Para producir un salto de línea es necesario terminar cada una de estas con dos espacios.

#### Ejemplo 1

```
Este es un salto de línea  
lo que produce quiere decir  
que se ha dado dos espacios  
al final de cada línea

```

lo que produce:

Este es un salto de línea  
lo que quiere decir  
que se han dado dos espacios  
al final de cada línea

Los versos en un texto poético pueden marcarse con un `\` (*backslash*) al final de cada verso:

#### Ejemplo 2

```
*Los sapos*

Trozos de barro,\
por la senda en penumbra,\
saltan los sapos.

&mdash; J. J. Tablada
```

Produce:

*Los sapos*

Trozos de barro,\
por la senda en penumbra,\
saltan los sapos.

&mdash; J. J. Tablada

---
Nota: Aunque estas dos formas no tienen una diferencia visual, es importante mencionar que el ejemplo del verso nos transmite un mayor control en los cortes de línea en caso de que estos sean constantes.

---

Los párrafos se delimitan al dar un espacio entre línea y línea.

```
Párrafo 1

Párrafo 2

Párrafo 3
```

## Hipertexto

### Notas a Pié de página

Existen dos formas de integrar notas a pie:

1. con un id
2. en línea

| Forma |
| :---: |
|  | 



[^1]: Solo se pueden marcar hasta dos niveles.

[^2]: Pimentel, L. A. (2006). Visión autoral/vision figural: una mirada desde la narratología y fenomenología. *Acta poética*, *27*(1), 245-271.
